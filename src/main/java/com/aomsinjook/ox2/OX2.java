/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aomsinjook.ox2;

/**
 *
 * @author cherz__n
 */
import java.util.Scanner;

public class OX2 {

    static boolean isFinish = false;
    static char winner = '-';
    static int row, col;
    static int countturn = 0;
    static Scanner sc = new Scanner(System.in);

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'},
    {'-', '-', '-'}};
    static char player = 'X';

    static void ShowWelcom() {
        System.out.println("Welcom to OX Game");
        System.out.println();

    }

    static void ShowTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);

            }
            System.out.println("");

        }

    }

    static void ShowTurn() {
        System.out.println(player + " turn");
    }

    static void ShowInput() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            if (countturn >= 8) {
                break;
            }

            System.out.println("Error");

        }
    }

    static void CheckRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void CheckColum() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void ShowDawe() {
        if (countturn >= 8) {
			winner = 'D';
			isFinish = true;

		}
    }

    static void CheckX1() {
        for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[j][j] != player) {
					return;

				}
			}
		}
		isFinish = true;
		winner = player;

	
    }

    static void CheckX2() {
        for (int i = 0; i < 3; i++) {
			for (int j = 0, k = 2; j < 3; j++, k--) {
				if (table[j][k] != player) {
					return;

				}
			}
		}
		isFinish = true;
		winner = player;
    }

    static void ShowCheckwin() {
        CheckRow();
        CheckColum();
        CheckX1();
        CheckX2();
        ShowDawe();

    }

    static void SwitchPlayer() {
        if (player == 'X') {
            player = 'O';

        } else {
            player = 'X';
        }
        countturn++;
    }

    static void ShowResult() {
        if (winner == 'D') {
            System.out.println("Draw!!");

        } else {
            System.out.println("Player " + winner + " Win!!!");

        }
    }

    static void ShowBye() {
        System.out.println("Bye bye ....");
    }

    public static void main(String[] args) {
        ShowWelcom();
        do {
            ShowTable();
            ShowTurn();
            ShowInput();
            ShowCheckwin();
            SwitchPlayer();
        } while (!isFinish);
        ShowResult();
        ShowBye();

    }
}
